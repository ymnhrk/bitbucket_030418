﻿$PBExportHeader$test_1.sra
$PBExportComments$Generated Application Object
forward
global type test_1 from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type test_1 from application
string appname = "test_1"
end type
global test_1 test_1

on test_1.create
appname = "test_1"
message = create message
sqlca = create transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on test_1.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

